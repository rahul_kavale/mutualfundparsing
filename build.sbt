import sbt._
import Keys._

name := "Mutual Fund Parsing"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "org.specs2" % "specs2_2.10" % "3.3.1" withSources() withJavadoc()

libraryDependencies += "junit" % "junit" % "4.10" withSources() withJavadoc()
