package com.rahulkavale.mutualFund

import java.util.Date

case class Vendor(name: String)

case class Scheme(code: String, divPayout: String, divReinvestment: String, name: String, category: SchemeCategory, var vendor: Vendor = null) {
  def setVendor(vendor: Vendor) = this.vendor = vendor
}

object Scheme {
  def apply(input: String, category: SchemeCategory): Option[Scheme] = {
    val split = input.split(";")
    split match {
      case _ if split.size == 1 => None
      case _ if split.size == 8 =>
        Some(new Scheme(split(0), split(1), split(2), split(3), category))
    }
  }
}

case class SchemeCategory(name: String)

case class PriceRecord(scheme: Scheme, netAssetValue: Option[Float], repurchasePrice: Option[Float], salePrice: Option[Float], date: Date)

object PriceRecord {
  def apply(input: String, scheme: Scheme): Option[PriceRecord] = {
    val split = input.split(";")
    split match {
      case _ if split.size == 1 => None
      case _ if split.size == 8 =>
        Some(new PriceRecord(scheme, parseFloat(split(4)), parseFloat(split(5)), parseFloat(split(6)), new Date(split(7))))
    }
  }

  def parseFloat(string: String): Option[Float] = try {
    Some(string.toFloat)
  } catch {
    case _ : NumberFormatException => None
  }
}

case class Header(header: String)
