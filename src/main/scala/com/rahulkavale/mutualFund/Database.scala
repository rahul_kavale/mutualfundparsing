package com.rahulkavale.mutualFund

import scala.collection.mutable.ListBuffer

object Database {
  val schemeCategories = ListBuffer[SchemeCategory]()
  val schemes = ListBuffer[Scheme]()
  val vendors = ListBuffer[Vendor]()
  val priceRecords = ListBuffer[PriceRecord]()

  def appendVendor(vendor: Vendor) = {
    if(!vendors.contains(vendor)) vendors.append(vendor)
  }
}
