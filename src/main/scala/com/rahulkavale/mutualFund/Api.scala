package com.rahulkavale.mutualFund

import java.util.Date
import com.rahulkavale.mutualFund.parsers.FileParser

object Api {
  def schemesFor(vendorName: String): List[String] = {
    Database
      .schemes
      .filter(_.vendor.name.toLowerCase.contains(vendorName.toLowerCase))
      .map(_.name)
      .toList
  }

  // /:vendor/:category/schemes GET
  def schemesFor(vendorName: String, category: String): List[Scheme] = {
    Database
      .schemes
      .filter(scheme =>
      scheme.vendor.name.toLowerCase.contains(vendorName.toLowerCase)
        && scheme.category.name.toLowerCase.contains(category.toLowerCase))
      .toList
  }

  //  /:vendor/:schemes?type=:type
  def schemesForType(vendor: String, schemeOptionType: String) = {
    Database
      .schemes
      .filter(_.vendor.name.toLowerCase.contains(vendor.toLowerCase))
      .filter(_.name.contains(schemeOptionType.toLowerCase))
      .map(_.name).toList
  }

  // /:scheme/:date/price
  def schemePriceFor(schemeName: String, date: String) = {
    Database
      .priceRecords
      .filter(priceRecord =>
      priceRecord.scheme.name.toLowerCase.contains(schemeName.toLowerCase)
        && priceRecord.date.equals(date))
  }

  def validPriceRecords = {
    Database
      .priceRecords
      .filter(priceRecords => priceRecords.netAssetValue.isDefined
      && priceRecords.repurchasePrice.isDefined &&
      priceRecords.salePrice.isDefined)
  }

  // /:category/:date/lowest GET
  def lowestPriceFor(category: String, date: String) = {
    val priceRecords = validPriceRecords //Database.priceRecords //.filter(_.date.equals(date)).toList
      .filter(_.scheme.category.name.toLowerCase.contains(category.toLowerCase))
      .sortBy(-_.netAssetValue.get)
      .headOption
  }

  // /:vendor/bestScheme
  def bestScheme(vendorName: String) = {
    Database
      .priceRecords
      .filter(_.scheme.vendor.name.toLowerCase.contains(vendorName.toLowerCase))
      .sortBy(_.netAssetValue)
      .head.scheme.name
  }

  // /:scheme/:date/sale-price GET
  def salePriceFor(name: String, date: Date) = {
    Database
      .priceRecords
      .filter(priceRecord =>
      priceRecord.scheme.name.toLowerCase.contains(name.toLowerCase)
        && priceRecord.date.equals(date))
  }

  // /:scheme/:date/buy-price GET
  def buyPriceFor(name: String, date: Date) = {
    Database
      .priceRecords
      .filter(priceRecord =>
      priceRecord.scheme.name.toLowerCase.contains(name.toLowerCase)
        && priceRecord.date.equals(date))
      .map(pr => (pr.scheme.name, pr.netAssetValue))
  }

  // /process?fileName=file  POST
  def processFile(path: String) = {
    FileParser.parseFile(path)
  }

  // /vendors
  def vendors = {
    Database
      .vendors
      .map(_.name)
  }

  // /schemes-categories
  def schemeCategories = {
    Database
      .schemeCategories
      .map(_.name)
  }

  // /:vendor/scheme-categories
  def schemeCategories(vendor: String) = {
    Database
      .schemes
      .filter(_.vendor.name.contains(vendor.toLowerCase))
      .map(_.category.name)
  }

  //  /most-profitable/:scheme-category
  def mostProfitable(category: String) = {
    validPriceRecords
      .filter(_.scheme.category.name.toLowerCase.contains(category.toLowerCase))
      .map(p => (p.scheme.name, p.salePrice.get - p.netAssetValue.get))
      .sortBy(-_._2).head
  }
}
