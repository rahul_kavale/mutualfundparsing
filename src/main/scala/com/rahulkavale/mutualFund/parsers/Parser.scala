package com.rahulkavale.mutualFund.parsers

import com.clearTax.mutualFund._
import com.rahulkavale.mutualFund._

case class ParseError(message: String)

case class ParseResult(schemeCategories: Set[SchemeCategory] = Set(),
                       vendors: Set[Vendor] = Set(),
                       schemes: Set[Scheme] = Set(),
                       priceRecords: Set[PriceRecord] = Set())

case class Context()

trait Parser[T, U, C] {
  def parse(input: T)(implicit context: C): Either[ParseError, U]
}

object Parser {
  val HEADER_ROW = "Scheme Code;ISIN Div Payout/ ISIN Growth;ISIN Div Reinvestment;Scheme Name;Net Asset Value;Repurchase Price;Sale Price;Date"
  implicit val headerParser = new Parser[String, Header, Context] {
    override def parse(input: String)(implicit context: Context): Either[ParseError, Header] = input match {
      case HEADER_ROW =>
        Right(Header(input))
      case _ => Left(ParseError("Invalid header found"))
    }
  }

  implicit val blankLineParser = new Parser[String, String, Context] {
    override def parse(input: String)(implicit context: Context): Either[ParseError, String] = input match {
      case _ if input.length == 0 =>
        Right(input)
      case _ => Left(ParseError("Not a blank line"))
    }
  }

  implicit val vendorParser = new Parser[String, Vendor, Context] {
    override def parse(input: String)(implicit context: Context): Either[ParseError, Vendor] = input match {
      case _ if input.endsWith("Mutual Fund") => Right(Vendor(input))
      case _ => Left(ParseError("Not a valid Vendor"))
    }
  }

  implicit val schemeCategoryParser = new Parser[String, SchemeCategory, Context] {
    override def parse(input: String)(implicit context: Context): Either[ParseError, SchemeCategory] = input match {
      case _ if input.matches( """^.*?Schemes\(\w+\)$""") => Right(SchemeCategory(input))
      case _ => Left(ParseError("not a valid scheme"))
    }
  }

  implicit val schemeParser = new Parser[String, Scheme, SchemeCategory] {
    override def parse(input: String)(implicit category: SchemeCategory): Either[ParseError, Scheme] = Scheme(input, category) match {
      case Some(scheme) => Right(scheme)
      case _ => Left(ParseError("not a valid scheme" + input))
    }
  }

  implicit val recordParser = new Parser[String, PriceRecord, Scheme] {
    override def parse(input: String)(implicit scheme: Scheme): Either[ParseError, PriceRecord] = PriceRecord(input, scheme) match {
      case Some(priceRecord) => Right(priceRecord)
      case None => Left(ParseError("Invalid Price Record" + input))
    }
  }

  def parse[T, U, C](input: T)(implicit parser: Parser[T, U, C], context: C): Either[ParseError, U] = {
    parser.parse(input)
  }

  implicit val defaultContext = new Context

  def parseLine(line: String)(implicit currentSchemeCategory: SchemeCategory, currentVendor: Vendor): (String, AnyRef) = {
    blankLineParser.parse(line) match {
      case Right(blankLine) => ("String", blankLine.asInstanceOf[AnyRef])
      case _ => headerParser.parse(line) match {
        case Right(result) => ("Header", result.asInstanceOf[AnyRef])
        case _ => schemeCategoryParser.parse(line) match {
          case Right(result) => ("SchemeCategory", result.asInstanceOf[AnyRef])
          case _ => vendorParser.parse(line) match {
            case Right(result) => ("Vendor", result.asInstanceOf[AnyRef])
            case _ => schemeParser.parse(line) match {
              case Right(scheme) =>
                scheme.setVendor(currentVendor)
                recordParser.parse(line)(scheme) match {
                  case Right(pr) => ("Scheme-PriceRecord-Tuple", (scheme, pr).asInstanceOf[AnyRef])
                  case _ => ("ParseError", ParseError("Not a valid record" + line).asInstanceOf[AnyRef])
                }
              case _ =>
                ("ParseError", ParseError("Not a valid record" + line).asInstanceOf[AnyRef])
            }
          }
        }
      }
    }
  }
}
