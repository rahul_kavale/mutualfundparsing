package com.rahulkavale.mutualFund.parsers

import java.io.{BufferedReader, FileInputStream, InputStreamReader}
import com.clearTax.mutualFund._
import Parser._
import com.rahulkavale.mutualFund._

object FileParser {

  def parseFile(path: String, count: Int = Integer.MAX_VALUE) = {
    val reader: BufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path)))
    var line: String = reader.readLine()
    implicit var currentSchemeCategory: SchemeCategory = null
    implicit var currentVendor: Vendor = null

    var tmpCount = 0
    while (line != null && tmpCount < count) {
      parseLine(line) match {
        case ("SchemeCategory", value) =>
          val elem: SchemeCategory = value.asInstanceOf[SchemeCategory]
          currentSchemeCategory = elem
          Database.schemeCategories.append(elem)

        case ("Scheme-PriceRecord-Tuple", value) =>
          val (scheme, priceRecord) = value.asInstanceOf[(Scheme, PriceRecord)]
          Database.schemes.append(scheme)
          Database.priceRecords.append(priceRecord)

        case ("Vendor", value) =>
          val elem: Vendor = value.asInstanceOf[Vendor]
          currentVendor = elem
          Database.appendVendor(elem)

        case (_, value) =>
          println("could not parse to any domain" + line)
      }
      line = reader.readLine()
      tmpCount += 1
    }
  }
}

