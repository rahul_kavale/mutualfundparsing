package com.rahulkavale.mutualFund.parsers

import com.clearTax.mutualFund._
import com.rahulkavale.mutualFund._
import org.specs2.mutable.Specification

class ParserTest extends Specification {

  implicit val context = new Context

  "Header Parser" should {
    "not parse wrong header" in {
      Parser.headerParser.parse("random header row content") shouldEqual Left(ParseError("Invalid header found"))
    }
    "parse correct header" in {
      Parser.headerParser.parse(Parser.HEADER_ROW) shouldEqual Right(Header(Parser.HEADER_ROW))
    }
  }

  "Vendor Parser" should {
    "parse vendors correctly" in {
      Parser.vendorParser.parse("Baroda Pioneer Mutual Fund") shouldEqual Right(Vendor("Baroda Pioneer Mutual Fund"))
    }
    "not parse wrong vendors" in {
      Parser.vendorParser.parse("Some vendor name not ending with mututal fund keyword") shouldEqual Left(ParseError("Not a valid Vendor"))
    }
  }
  "Scheme category Parser" should {
    "parse scheme categories correctly" in {
      Parser.schemeCategoryParser.parse("Open Ended Schemes(Balanced)") shouldEqual Right(SchemeCategory("Open Ended Schemes(Balanced)"))
    }
    "not parse wrong scheme categories " in {
      Parser.schemeCategoryParser.parse("Some vendor name not ending with mututal fund keyword") shouldEqual Left(ParseError("not a valid scheme"))
    }
  }

  val vendor = new Vendor("Baroda Pioneer Mutual Fund")
  val schemeCategory = new SchemeCategory("Open Ended Schemes(Balanced)")
  "Scheme Parser" should {
    "parse schemes correctly" in {
      val input = "120523;INF846K01ET8;INF846K01EU6;Axis Triple Advantage Fund - Direct Plan - Dividend Option;13.4000;13.2660;13.4000;25-Jun-2015"
      Parser.schemeParser.parse(input)(schemeCategory) shouldEqual Right(Scheme("120523", "INF846K01ET8", "INF846K01EU6", "Axis Triple Advantage Fund - Direct Plan - Dividend Option", SchemeCategory("Open Ended Schemes(Balanced)"), null))
    }
  }

  "Price Record Parser" should {
    val scheme = Scheme("", "", "", "", schemeCategory, vendor)
    "parse schemes correctly" in {
      val input = "120523;INF846K01ET8;INF846K01EU6;Axis Triple Advantage Fund - Direct Plan - Dividend Option;13.4000;13.2660;13.4000;25-Jun-2015"
      Parser.recordParser.parse(input)(scheme) shouldEqual Right(PriceRecord(scheme, Some(13.4f), Some(13.266f), Some(13.4f), new java.util.Date("25-Jun-2015")))
    }
  }

}
