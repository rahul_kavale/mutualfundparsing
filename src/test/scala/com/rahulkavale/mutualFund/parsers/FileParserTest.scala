package com.rahulkavale.mutualFund.parsers

import com.clearTax.mutualFund.Database
import com.rahulkavale.mutualFund.Database
import org.specs2.mutable._

class FileParserTest extends Specification {

  "File Parser" should {
    "parse input file containing mutual fund details" in {
      FileParser.parseFile("src/test/resources/mutualFundSampleData.txt")

      Database.priceRecords.size shouldEqual 19
      Database.schemes.size shouldEqual 19
      Database.schemeCategories.size shouldEqual 2
      Database.vendors.size shouldEqual 2
    }
  }
}
