import java.util.Date

import com.clearTax.mutualFund._
import com.rahulkavale.mutualFund.Api
import com.rahulkavale.mutualFund.parsers.{FileParser, Parser}
import Parser._
import com.clearTax.mutualFund.parsers._

import scala.collection.mutable.ListBuffer

//implicit val defaultContext = new com.clearTax.mutualFund.parsers.Context
//
//val input1 = "Scheme Code;ISIN Div Payout/ ISIN Growth;ISIN Div Reinvestment;Scheme Name;Net Asset Value;Repurchase Price;Sale Price;Date"
//
//Parser.parse[String, Header, Context](input1)
//
//val input2 = "Open Ended Schemes(Balanced)"
//val category = Parser.parse[String, SchemeCategory, Context](input2) match {
//  case Right(schCat) => schCat
//}
//
//val input3 = "Axis Mutual Fund"
//Parser.parse[String, Vendor, Context](input3)
//
//val input4 = "120523;INF846K01ET8;INF846K01EU6;Axis Triple Advantage Fund - Direct Plan - Dividend Option;13.4000;13.2660;13.4000;25-Jun-2015"
//implicit val schemeCategory = category
//
//implicit val scheme = Parser.parse[String, Scheme, SchemeCategory](input4) match {
//  case Right(sch) => sch
//}
//
//
//Parser.parse[String, PriceRecord, Scheme](input4) match {
//  case Right(priceRecord) => priceRecord
//}
//
//val lines =
//  """
//    |Scheme Code;ISIN Div Payout/ ISIN Growth;ISIN Div Reinvestment;Scheme Name;Net Asset Value;Repurchase Price;Sale Price;Date
//    |Open Ended Schemes(Balanced)
//    |Axis Mutual Fund
//    |
//    |120523;INF846K01ET8;INF846K01EU6;Axis Triple Advantage Fund - Direct Plan - Dividend Option;13.4000;13.2660;13.4000;25-Jun-2015
//    |120524;INF846K01EV4;-;Axis Triple Advantage Fund - Direct Plan - Growth Option;15.5586;15.4030;15.5586;25-Jun-2015
//    |113065;INF846K01776;INF846K01784;Axis Triple Advantage Fund - Dividend Option;12.3635;12.2399;12.3635;25-Jun-2015
//    |113064;INF846K01768;-;Axis Triple Advantage Fund - Growth Option;15.1587;15.0071;15.1587;25-Jun-2015
//    |
//    |Baroda Pioneer Mutual Fund
//    |
//    |125112;INF955L01DB5;-;Baroda Pioneer Balance Fund - Plan A - Bonus Option;43.05;42.62;43.05;25-Jun-2015
//    |101913;INF955L01682;INF955L01690;BARODA PIONEER BALANCE FUND - Plan A - Dividend Option;25.60;25.34;25.60;25-Jun-2015
//    |101912;INF955L01708;-;BARODA PIONEER BALANCE FUND - Plan A - Growth Option;43.05;42.62;43.05;25-Jun-2015
//    |119325;INF955L01922;-;BARODA PIONEER BALANCE FUND - Plan B (Direct) - Dividend Option;26.35;26.09;26.35;25-Jun-2015
//    |119326;INF955L01948;-;BARODA PIONEER BALANCE FUND - Plan B (Direct) - Growth Option;44.08;43.64;44.08;25-Jun-2015
//    |Open Ended Schemes(ELSS)
//    |
//    |Axis Mutual Fund
//    |
//    |120502;INF846K01EX0;INF846K01EY8;Axis Long Term Equity Fund - Direct Plan - Dividend option;27.0912;27.0912;27.0912;25-Jun-2015
//    |120503;INF846K01EW2;-;Axis Long Term Equity Fund - Direct Plan - Growth Option;31.7998;31.7998;31.7998;25-Jun-2015
//    |112322;INF846K01149;INF846K01156;Axis Long Term Equity Fund - Dividend;22.5589;22.5589;22.5589;25-Jun-2015
//    |112323;INF846K01131;-;Axis Long Term Equity Fund - Growth;30.8413;30.8413;30.8413;25-Jun-2015
//    |
//    |Baroda Pioneer Mutual Fund
//    |
//    |100068;INF955L01641;-;BARODA PIONEER ELSS 96 - Plan A - Dividend Payout Option;31.11;31.11;31.11;25-Jun-2015
//    |119339;INF955L01914;-;BARODA PIONEER ELSS 96 - Plan B (Direct) - Dividend Payout Option;32.11;32.11;32.11;25-Jun-2015
//    |125111;INF955L01DA7;-;Baroda Pioneer ELSS 96 - Plan B (Direct)- Bonus Option;37.56;37.56;37.56;25-Jun-2015
//    |125110;INF955L01CZ6;-;Baroda Pioneer ELSS 96 Fund - Plan A- Bonus Option;36.20;36.20;36.20;25-Jun-2015
//    |134044;INF955L01GD4;-;Baroda Pioneer ELSS 96 Plan A -Growth Option;36.20;36.20;36.20;25-Jun-2015
//    |134045;INF955L01GE2;-;Baroda Pioneer ELSS 96 Plan B(Direct) -Growth Option;37.56;37.56;37.56;25-Jun-2015
//  """.stripMargin

//val parseResult = parseLines(List(lines.split("\n"): _*))
//println("price records")
//parseResult._4.size
//println("scheme categories")
//parseResult._2.foreach(println(_))
//println("vendors")
//parseResult._3.foreach(println(_))
//println("schems")
//parseResult._4.foreach(println(_))
//FileParser.parseFileInStream("/Users/rahulkavle/Desktop/NAV0.txt", 100)
//Database.priceRecords.foreach(println(_))
//Database.schemes.foreach(println(_))
//Database.schemeCategories.foreach(println(_))
//Database.vendors.foreach(println(_))
// /:vendor/schemes GET
FileParser.parseFile("/Users/rahulkavle/Desktop/NAV0.txt")
//Database.priceRecords.size
//Database.schemes.size
//Database.schemeCategories.size
//Database.vendors.size
Api.schemesFor("Baroda Pioneer Mutual Fund")
Api.schemesFor("Baroda Pioneer Mutual Fund", "Open Ended Schemes")
Api.bestScheme("Axis Mutual Fund")
